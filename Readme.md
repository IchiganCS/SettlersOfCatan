# Settlers of Catan

 The game "Settlers of Catan" was originally designed by Klaus Teuber and is particularly well-known in Germany. It has brought me several hours of joy and seemed complex enough to pose an interesting challenge.

 That's why I decided to make a computer game out of it.

## How it works

The entire game works with OpenGL in .NET 6 - yes, that is weird, but remember: this is a project just for learning: The OpenGL calls are compleletly equivalent (thanks to OpenTK) and the performance hit from C# is insignificant for this application.

The game logic itself is handled with graphs. Take a look at BoardGraph to understand this.

## Yet to come

It's yet to be seen how far this will go - in particular, how the bots will be implemented. Hot seat is planned to be implemented and maybe a P2P online multiplayer? That would be interesting, definitely!

Another feature I want to add are custom scenarios: You should be able to create a file describing a specific scenario which then can be read and a world according to these rules can be generated. Not trivial for complex scenarios, there is an open issue, if you have a nice idea for such a file format.

Currently, this project is just handled by me, do with the code whatever you want, you may even help out with game logic stuff etc., if you want.

## Sources

For most textures - <https://pdfslide.tips/download/link/catan-imprimir>

All the textures which are not in this pdf-file were created by me in Paint 3D
