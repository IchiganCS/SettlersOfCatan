﻿using OpenTK.Mathematics;
using SettlersOfCatan.Graphics;

namespace SettlersOfCatan.Logic;

public abstract class Node
{
    private int TraversalID { get; set; }
    private int RenderID { get; set; }
    public int RecalculationID { get; set; }

    public IRenderer Renderer { get; set; }

    public bool IsPointInShape(Vector2 point)
        => Renderer.IsPointInShape(point);

    public abstract List<Node> GetAllNeighbors();


    public void Render(int id)
    {
        if (RenderID == id)
            return;
        RenderID = id;
        Renderer.Render();
        GetAllNeighbors().ForEach(x => x?.Render(id));
    }

    public void Traverse(Action<Node> action, int id)
    {
        if (TraversalID == id)
            return;

        action(this);
        TraversalID = id;

        GetAllNeighbors().ForEach(x => x?.Traverse(action, id));
    }
}