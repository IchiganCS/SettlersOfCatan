﻿using OpenTK.Mathematics;
using SettlersOfCatan.Graphics;

namespace SettlersOfCatan.Logic;

public class TileNode : Node
{
    public Tile Tile
    {
        get => ((TileRenderer)Renderer).Tile;
        set => ((TileRenderer)Renderer).Tile = value;
    }

    public TileNode()
    {
        Renderer = new TileRenderer();
    }



    public StreetNode[] StreetNodes { get; set; } = new StreetNode[6];
    public JunctionNode[] JunctionNodes { get; set; } = new JunctionNode[6];

    public void ConnectStreet(StreetNode street, int orientation)
    {
        int index = orientation < 2 ? 0 : 1;
        if (orientation == 5) index = 0;

        StreetNodes[orientation] = street;
        street.TileNodes[index] = this;
    }
    public void ConnectJunction(JunctionNode junction, int orientation)
    {
        JunctionNodes[orientation] = junction;

        int index = (orientation == 3 || orientation == 2) ? 0 : 1;
        if (orientation < 2) index = 2;

        junction.TileNodes[index] = this;
    }
    /// <summary>
    /// This adds a street between the tiles (at this.StreetNodes[orientation]) and looks for exisiting Junctions, otherwise
    /// adds new Junctions
    /// </summary>
    /// <param name="other"></param>
    /// <param name="orientation"></param>
    public void ExtendendConnectTile(TileNode other, int orientation)
    {
        int invertOrient = BoardGraph.InvertOrientation(orientation);
        StreetNode street = new();
        ConnectStreet(street, orientation);
        other.ConnectStreet(street, invertOrient);

        //find junctions or create
        //these are the indices to get the required junctions from one tile's list
        int thisLeft = ((orientation + 1) % 6 < 3) ? (orientation + 5) % 6 : orientation;
        int thisRight = ((orientation + 1) % 6 < 3) ? orientation : (orientation + 5) % 6;
        int otherLeft = ((invertOrient + 1) % 6 < 3) ? (invertOrient + 5) % 6 : invertOrient;
        int otherRight = ((invertOrient + 1) % 6 < 3) ? invertOrient : (invertOrient + 5) % 6;

        JunctionNode left = JunctionNodes[thisLeft];
        JunctionNode right = JunctionNodes[thisRight];
        if (left is null)
            left = other.JunctionNodes[otherLeft];
        else
            left.MergeWith(other.JunctionNodes[otherLeft]);
        if (right is null)
            right = other.JunctionNodes[otherRight];
        else
            right.MergeWith(other.JunctionNodes[otherRight]);

        left ??= new();
        right ??= new();

        ConnectJunction(right, thisRight);
        ConnectJunction(left, thisLeft);
        other.ConnectJunction(right, otherRight);
        other.ConnectJunction(left, otherLeft);

        if ((orientation + 1) % 6 < 3)
        {
            street.ConnectJunction(right, orientation);
            street.ConnectJunction(left, invertOrient);
        }
        else
        {
            street.ConnectJunction(right, invertOrient);
            street.ConnectJunction(left, orientation);
        }
    }
    public TileNode GetAdjacentTileAt(int orientation)
        => StreetNodes[orientation]?.GetOppositeTile(this);
    public bool HasAdjacentTileAt(int orientation)
        => StreetNodes[orientation] is not null && StreetNodes[orientation].GetOppositeTile(this) is not null;
    public List<TileNode> GetAllAdjacentTiles()
        => Enumerable.Range(0, 6).Select(GetAdjacentTileAt).ToList();
    public bool IsAdjacentTo(TileNode node)
        => ReferenceEquals(node, this);
    public override List<Node> GetAllNeighbors()
    {
        List<Node> nodes = new(StreetNodes);
        nodes.AddRange(JunctionNodes);
        return nodes;
    }

    public void Recalculate(int recalcID)
    {
        if (RecalculationID == recalcID)
            return;
        RecalculationID = recalcID;

        float tileHeight = 2 * (float)Math.Cos(30f / 180f * Math.PI); //because of the hexagonal shape

        Vector2 streetTile = new(0, -tileHeight / 2);
        Vector2 streetJunction = new(0.5f, 0);
        Vector2 tileJunction = streetTile + streetJunction;


        for (int i = 0; i < StreetNodes.Length; i++)
        {
            if (StreetNodes[i] is null || StreetNodes[i].RecalculationID == RecalculationID)
                continue;

            float angleRad = i * 60f / 180f * (float)Math.PI;
            Matrix4 rot = Matrix4.CreateRotationZ(angleRad);
            StreetNodes[i].Renderer.Transform = Matrix4.CreateTranslation(new(streetTile)) * rot * Renderer.Transform;
            StreetNodes[i].RecalculationID = RecalculationID;
        }

        for (int i = 0; i < JunctionNodes.Length; i++)
        {
            if (JunctionNodes[i] is null || JunctionNodes[i].RecalculationID == RecalculationID)
                continue;

            float angleRad = i * 60f / 180f * (float)Math.PI;
            Matrix4 rot = Matrix4.CreateRotationZ(angleRad);
            JunctionNodes[i].Renderer.Transform = rot.Inverted() * Matrix4.CreateTranslation(new(tileJunction)) * rot * Renderer.Transform;
            JunctionNodes[i].RecalculationID = RecalculationID;
        }

        for (int i = 0; i < StreetNodes.Length; i++)
        {
            if (StreetNodes[i] is not null)
            {
                TileNode tile = StreetNodes[i].GetOppositeTile(this);
                if (tile.RecalculationID == RecalculationID)
                    continue;
                float angleRad = i * 60f / 180f * (float)Math.PI;
                Matrix4 rot = Matrix4.CreateRotationZ(angleRad);
                tile.Renderer.Transform = rot.Inverted() * Matrix4.CreateTranslation(2 * new Vector3(streetTile)) * rot * Renderer.Transform;

                if (tile is not null) tile.Recalculate(RecalculationID);
            }
        }

        Renderer.Transform = Matrix4.CreateRotationZ(Tile.Rotation * 60f / 180f * (float)Math.PI) * Renderer.Transform;
    }

    public override string ToString()
    {
        return Tile.ToString();
    }
}