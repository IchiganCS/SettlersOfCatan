﻿using OpenTK.Mathematics;
using SettlersOfCatan.Graphics;

namespace SettlersOfCatan.Logic;

public class StreetNode : Node
{
    public StreetNode()
    {
        Renderer = new StreetRenderer() { StreetObject = new() };
    }

    public Street Street
    {
        get => (Renderer as StreetRenderer)?.StreetObject;
        set => (Renderer as StreetRenderer)!.StreetObject = value;
    }






    //the left one is index 0, the lower is 1
    public JunctionNode[] JunctionNodes { get; set; } = new JunctionNode[2];
    //the upper one is index 0, the lower is 1
    public TileNode[] TileNodes { get; set; } = new TileNode[2];


    //use the orientation around std grid: the junction is "right" from the street at 0, with one the next side is chosen
    public void ConnectJunction(JunctionNode junction, int orientation)
    {
        int index = (orientation > 1 && orientation < 5) ? 0 : 1;
        JunctionNodes[index] = junction;

        if (orientation == 1 || orientation == 2)
            junction.StreetNodes[0] = this;
        else if (orientation == 3 || orientation == 4)
            junction.StreetNodes[1] = this;
        else
            junction.StreetNodes[2] = this;
    }
    public void ConnectTile(TileNode node, int orientation)
    {
        node.ConnectStreet(this, BoardGraph.InvertOrientation(orientation));
    }
    public TileNode GetOppositeTile(TileNode node)
    {
        if (ReferenceEquals(node, TileNodes[0]))
            return TileNodes[1];
        return TileNodes[0];
    }
    public override List<Node> GetAllNeighbors()
    {
        List<Node> nodes = new(JunctionNodes);
        nodes.AddRange(TileNodes);
        return nodes;
    }
}