﻿using SettlersOfCatan.Graphics;

namespace SettlersOfCatan.Logic;

public class JunctionNode : Node
{
    public JunctionNode()
    {
        Renderer = new JunctionRenderer() { JunctionObject = new() };
    }

    public Junction Junction
    {
        get => (Renderer as JunctionRenderer)?.JunctionObject;
        set => (Renderer as JunctionRenderer)!.JunctionObject = value;
    }





    //for both of these: below - right - left
    public StreetNode[] StreetNodes { get; set; } = new StreetNode[3];
    public TileNode[] TileNodes { get; set; } = new TileNode[3];

    public void MergeWith(JunctionNode other)
    {
        if (other is null)
            return;

        for (int i = 0; i < StreetNodes.Length; i++)
        {
            if (StreetNodes[i] is null)
                StreetNodes[i] = other.StreetNodes[i];
            if (TileNodes[i] is null)
                TileNodes[i] = other.TileNodes[i];
        }
    }
    public void ConnectTile(TileNode tile, int orientation)
        => tile.ConnectJunction(this, orientation);
    public void ConnectStreet(StreetNode street, int orientation)
        => street.ConnectJunction(this, orientation);
    public override List<Node> GetAllNeighbors()
    {
        List<Node> nodes = new(StreetNodes);
        nodes.AddRange(TileNodes);
        return nodes;
    }
}