﻿namespace SettlersOfCatan.Logic;

public enum Resource
{
    Lumber = 0,
    Brick = 1,
    Sheep = 2,
    Wheat = 3,
    Ore = 4
}