﻿namespace SettlersOfCatan.Logic;

public class Tile
{
    public Resource? Resource { get; set; }
    public bool IsResource => Resource is not null;
    public int Number { get; set; } = -1;
    public bool HasRobber { get; set; } = false;

    public bool IsDesert { get; set; } = false;

    /// <summary>
    /// 5 is 3:1, else use Resource int
    /// </summary>
    public int? Port { get; set; }
    public bool IsPort => Port is not null;
    /// <summary>
    /// 0 is down, then counter-clockwise (so down-right is 1)
    /// </summary>
    public int Rotation { get; set; } = 0;

    public bool IsOcean { get; set; } = false;

    public bool IsSelected { get; set; } = false;

    public override string ToString()
    {
        if (IsResource)
            return $"Resource {Resource} with Number {Number}, {(HasRobber ? "with" : "no")} Robber";
        if (IsDesert)
            return $"Desert";
        if (IsPort)
            return $"Port {(Port == 5 ? "General" : ((Resource)Port!).ToString())}";
        if (IsOcean)
            return "Ocean";
        return "???????";
    }
}