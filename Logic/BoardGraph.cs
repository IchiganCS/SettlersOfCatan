﻿using OpenTK.Mathematics;
using SettlersOfCatan.Misc;

namespace SettlersOfCatan.Logic;

public class BoardGraph
{
    public TileNode Root { get; private set; }

    public int HorizontalDimension { get; private set; }
    public int VerticalDimension { get; private set; }

    public static int InvertOrientation(int input)
    {
        return (input + 3) % 6;
    }
    public static int MakeValidOrientation(int input)
    {
        return (input + 40 * 6) % 6;
    }

    public static BoardGraph GenerateDefault()
    {
        int seed = Guid.NewGuid().GetHashCode();
        Logger.Info($"Generating default board with seed {seed}");
        Random random = new(seed);


        List<int> numbers = new(new[]
        {
            5, 2, 6, 3, 8, 10, 9, 12, 11, 4, 8, 10, 9, 4, 5, 6, 3, 11
        });
        //insert desert
        numbers.Insert(random.Next(numbers.Count), -1);
        int numberIter = numbers.Count - 1;
        int[] resourceCount = new int[] { 4, 4, 4, 3, 3 };
        resourceCount = resourceCount.OrderBy(x => random.Next()).ToArray();

        Tile[] oceanArr = Enumerable.Range(0, 5)
            .Select(x => new Tile() { Port = x })
            .OrderBy(x => random.Next())
            .SelectMany(x => new Tile[] { x, new() { Port = 5 } })
            .SkipLast(1)
            //this generates an array which could look like sheep - general - brick - general... - ore
            //the next line adds an ocean tile in between
            .SelectMany(x => new Tile[] { x, new() { IsOcean = true } }).ToArray();
        int oceanStart = random.Next(oceanArr.Length);

        //starting in the middle circling outwards counterclockwise
        //if overflowing, ocean tiles will be endlessly returned
        TileNode getNextTileNode()
        {
            if (numberIter >= 0)
            {
                Tile res = new()
                {
                    Number = numbers[numberIter--]
                };

                if (res.Number != -1)
                {
                    //pick resource
                    int val = random.Next(resourceCount.Length);
                    while (resourceCount[val] == 0)
                        val = random.Next(resourceCount.Length);

                    res.Resource = (Resource)val;
                    resourceCount[val]--;
                }
                else
                    res.IsDesert = true;
                return new() { Tile = res };
            }

            return new() { Tile = oceanArr[(++oceanStart) % oceanArr.Length] };
        }




        TileNode middleTile = getNextTileNode();
        int startingOrient = random.Next(6);
        TileNode lastTile = null;
        TileNode first = null;

        //inner circle
        for (int i = startingOrient; i < startingOrient + 6; i++)
        {
            TileNode next = getNextTileNode();
            if (i == startingOrient)
                first = next;
        
            middleTile.ExtendendConnectTile(next, MakeValidOrientation(i));
        
            if (lastTile is not null)
                lastTile.ExtendendConnectTile(next, MakeValidOrientation(i + 1));
        
            lastTile = next;
        }
        lastTile!.ExtendendConnectTile(first, MakeValidOrientation(startingOrient + 1));


        //used for outer land and ocean tiles
        void CircleAround(int sideLength, ref TileNode lastInner)
        {
            TileNode last = null;
            TileNode first = null;

            TileNode nextInner = lastInner.GetAdjacentTileAt(MakeValidOrientation(startingOrient + 1));

            for (int i = startingOrient; i < startingOrient + 6; i++)
            {
                for (int j = 0; j < sideLength - 1; j++)
                {
                    TileNode next = getNextTileNode();
                    first ??= next;

                    if (last is not null)
                        last.ExtendendConnectTile(next, MakeValidOrientation(i + 1));
                    lastInner.ExtendendConnectTile(next, MakeValidOrientation(i));

                    bool turning = j == sideLength - 2;

                    if (!turning)
                    {
                        nextInner.ExtendendConnectTile(next, MakeValidOrientation(i - 1));

                        lastInner = nextInner;
                        if (j < sideLength - 3)
                            nextInner = nextInner.GetAdjacentTileAt(MakeValidOrientation(i + 1));
                    }
                    else
                    {
                        nextInner = lastInner.GetAdjacentTileAt(MakeValidOrientation(i + 2));
                    }


                    last = next;

                    //ocean tiles need rotation
                    if (sideLength == 4)
                    {
                        int[] validOrienations = Enumerable.Range(0, 6)
                            .Where(x => {
                                if (!next.HasAdjacentTileAt(x))
                                    return false;
                                TileNode node = next.GetAdjacentTileAt(x);
                                return !node.Tile.IsOcean && !node.Tile.IsPort;
                            }).ToArray();

                        next.Tile.Rotation = validOrienations[random.Next(validOrienations.Length)];
                    }
                }
            }


            first!.ExtendendConnectTile(last, MakeValidOrientation(startingOrient - 2));
            lastInner = last;
        }

        CircleAround(3, ref lastTile);
        CircleAround(4, ref lastTile);

        return new()
        {
            HorizontalDimension = 7,
            VerticalDimension = 7,
            Root = middleTile,
        };
    }

    public void Render()
        => Root.Render(Guid.NewGuid().GetHashCode());


    public List<Node> GetSelectedNodes(Vector2 point)
    {
        List<Node> res = new();
        Root.Traverse(current =>
        {
            if (current.IsPointInShape(point))
                res.Add(current);
        }, Guid.NewGuid().GetHashCode());
        return res;
    }
    public List<object> GetSelectedElements(Vector2 point)
    {
        List<Node> nodes = GetSelectedNodes(point);
        List<object> res = new();
        foreach(Node node in nodes) {
            if (node is TileNode tile)
                res.Add(tile.Tile);
            else if (node is StreetNode street)
                res.Add(street.Street);
            else if (node is JunctionNode junction)
                res.Add(junction.Junction);
        }
        return res;
    }

    public Vector2 Size { get; set; }
    public Vector2 Offset { get; set; }

    public void Recalculate()
    {
        float tileHeight = 2 * (float)Math.Cos(30f / 180f * Math.PI); //because of the hexagonal shape
        float tileWidth = 1 + (float)Math.Cos(60f / 180f * Math.PI);
        float usedWidth = HorizontalDimension * tileWidth + (float)Math.Cos(60f / 180f * Math.PI);
        float minXScaling = Size.X / usedWidth;
        float usedHeight = (2 * VerticalDimension + 1) * tileHeight / 2;
        float minYScaling = Size.Y / usedHeight;
        float scale = Math.Min(minXScaling, minYScaling);

        Vector2 off = new(Offset.X + scale, Offset.Y + scale * tileHeight / 2);

        //centering
        if (minYScaling < minXScaling)
            off.X += (Size.X - (float)(scale * usedWidth)) / 2;
        else
            off.Y += (Size.Y - (float)(scale * usedHeight)) / 2;

        Vector2 center = off + new Vector2(HorizontalDimension / 2f * tileWidth * scale, VerticalDimension / 2f * tileHeight * scale);

        Matrix4 scaleMat = Matrix4.CreateScale(scale, scale, 1);
        Root.Renderer.Transform = Matrix4.CreateTranslation(new(center)) * scaleMat;
        Root.Recalculate(Guid.NewGuid().GetHashCode());
    }
}