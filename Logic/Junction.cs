﻿namespace SettlersOfCatan.Logic;

public class Junction
{
    public Player Owner { get; set; }
    
    public bool IsHouse { get; set; }
    public bool IsCity { get; set; }

    public bool Unused
        => Owner is null || (!IsHouse && !IsCity);
}