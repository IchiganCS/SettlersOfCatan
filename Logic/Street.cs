﻿namespace SettlersOfCatan.Logic;

public class Street
{
    public Player Owner { get; set; }

    public bool IsStreet { get; set; }
    public bool Unused =>
        Owner is null || !IsStreet;
}