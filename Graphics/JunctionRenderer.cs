﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using SettlersOfCatan.Graphics.Shaders;
using SettlersOfCatan.Logic;

namespace SettlersOfCatan.Graphics
{
    public class JunctionRenderer : IRenderer
    {
        public Matrix4 TransformField;
        public Matrix4 Transform { get => TransformField; set => TransformField = value; }
        public Junction JunctionObject { get; set; }

        private static Mesh House { get; set; }

        public static void Init()
        {
            House = Mesh.LoadFromOFF("house.off", 0.5f);
            House.SetAttributes<BoardShader>();


        }
        public static void Destroy()
        {
            House.Dispose();
        }
        
        public bool IsPointInShape(Vector2 point)
        {
            if (JunctionObject is null)
                return false;

            Matrix4 funny = Transform; //the funny thing about this is that I don't know
            funny.Transpose(); //why this needs to be transposed (¯\_(ツ)_/¯)
            Vector2 center = (funny * new Vector4(0, 0, 0, 1)).Xy;
            Vector2 right = (funny * new Vector4(1, 0, 0, 1)).Xy;
            Vector2 centeredPoint = point - center;
            float scale = (right - center).Length;

            return centeredPoint.Length < scale / 3.5f;
        }

        public void Render()
        {
            if (JunctionObject.Unused)
                return;

            GL.UniformMatrix4(Shader.Board.TransformUniform, false, ref TransformField);
            GL.Uniform1(Shader.Board.IsPlayerObjectUniform, 1);
            GL.Uniform4(Shader.Board.PlayerColorUnifom, JunctionObject.Owner.Color);

            GL.Uniform1(Shader.Board.ZValueUnifom, -0.7f);

            if (JunctionObject.IsHouse)
                House.DrawElements();
        }
    }
}
