﻿using OpenTK.Graphics.OpenGL;
using SettlersOfCatan.Misc;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.PixelFormats;

namespace SettlersOfCatan.Graphics;

public sealed class Texture : IDisposable
{
    private int Handle { get; set; }

    public void ProcessImage(Image<Rgba32> img)
    {
        PixelFormat format = PixelFormat.Rgba;
        img.Mutate(x => x.Flip(FlipMode.Vertical));

        Handle = GL.GenTexture();
        GL.ActiveTexture(TextureUnit.Texture0);
        GL.BindTexture(TextureTarget.Texture2D, Handle);

        if (!img.TryGetSinglePixelSpan(out var span))
            Logger.Error("couldn't get pixel span");

        GL.TexImage2D(TextureTarget.Texture2D, 0,
            PixelInternalFormat.Rgba, img.Width, img.Height, 0,
            format, PixelType.UnsignedByte, span.ToArray());
        GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.MirroredRepeat);
        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.MirroredRepeat);
    }

    public Texture(string filename)
    {
        Logger.Info($"loading texture \"{filename}\"");
        filename = ResourceManager.GetFileName(filename, ResourceManager.FileType.Texture);
        if (!File.Exists(filename))
        {
            Logger.Warn($"couldn't find file {filename}");
            return;
        }
        using Image<Rgba32> img = Image.Load<Rgba32>(filename);
        ProcessImage(img);
    }

    public Texture(Image<Rgba32> image)
    {
        ProcessImage(image);
    }

    public void Bind()
    {
        GL.ActiveTexture(TextureUnit.Texture0);
        GL.BindTexture(TextureTarget.Texture2D, Handle);
    }

    public void Dispose()
    {
        Logger.Info("disposed a texture object");
        if (Handle != -1)
        {
            GL.DeleteTexture(Handle);
            Handle = -1;
        }
        else
            Logger.Warn("Called dispose on disposed texture");
    }
}