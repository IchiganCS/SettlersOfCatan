﻿using SettlersOfCatan.Misc;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Drawing.Processing;
using OpenTK.Mathematics;
using SettlersOfCatan.Graphics.Shaders;
using OpenTK.Graphics.OpenGL;

namespace SettlersOfCatan.Graphics;

public class TextRenderer
{
    private static FontCollection FontCollection { get; set; }
    private static Mesh TextBoxMesh { get; set; }

    private FontFamily Family { get; set; }

    public static void Init()
    {
        FontCollection = new();
        FontCollection.Install(ResourceManager.GetFileName("ShadowsIntoLight-Regular.ttf", ResourceManager.FileType.Font));
        FontCollection.Install(ResourceManager.GetFileName("times-new-roman.ttf", ResourceManager.FileType.Font));

        TextBoxMesh = new(new[]
        {
            new Vertex() { Position = new(-0.5f, -0.5f), TexCoord = new(0, 0) },
            new Vertex() { Position = new(+0.5f, -0.5f), TexCoord = new(1, 0) },
            new Vertex() { Position = new(+0.5f, +0.5f), TexCoord = new(1, 1) },
            new Vertex() { Position = new(-0.5f, +0.5f), TexCoord = new(0, 1) },
        }, new[] { 0, 1, 2, 0, 2, 3 });
        TextBoxMesh.SetAttributes<BoardShader>();
    }

    public TextRenderer(string fontName)
    {
        if (FontCollection.TryFind(fontName, out FontFamily family))
        {
            Family = family;
        }
        else
            Logger.Warn($"Couldn't find {fontName} as installed font family");
    }

    public Texture MakeCenteredText(string text, float scale, int resolution, Color4 textColor, Color4 backgroundColor)
    {
        using Image<Rgba32> img = new(resolution, resolution);

        static Color ConvertColor(Color4 color4)
        {
            return Color.FromRgba((byte)(color4.R * 255), (byte)(color4.G * 255), (byte)(color4.B * 255), (byte)(color4.A * 255));
        }


        Font font = Family.CreateFont(resolution / scale);
        RendererOptions options = new(font)
        {
            WrappingWidth = 0,
        };
        FontRectangle rectangle = TextMeasurer.Measure(text, options);
        float adjust = Math.Max(rectangle.Width, rectangle.Height);
        img.Mutate(x => x.Fill(ConvertColor(backgroundColor))
                         .DrawText(new DrawingOptions() { Transform = System.Numerics.Matrix3x2.CreateScale(adjust) }, text, font, ConvertColor(textColor), new((resolution + rectangle.X / 2 - rectangle.Width) / 2 / adjust,
                                                                            (resolution + rectangle.Y / 2 - rectangle.Height) / 2 / adjust)));

        return new(img);
    }

    public static void Render(Texture text, Matrix4 Transform)
    {
        GL.Uniform1(Shader.Board.TileSelectedUniform, 0);
        GL.Uniform1(Shader.Board.ZValueUnifom, -0.99f);
        GL.Uniform1(Shader.Board.IsPlayerObjectUniform, 0);
        GL.UniformMatrix4(Shader.Board.TransformUniform, false, ref Transform);

        text.Bind();
        TextBoxMesh.DrawElements();
    }
}