﻿using OpenTK.Mathematics;

namespace SettlersOfCatan.Graphics;

public interface IRenderer
{
    public Matrix4 Transform { get; set; }
    public void Render();
    public bool IsPointInShape(Vector2 point);
}