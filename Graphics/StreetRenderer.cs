﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using SettlersOfCatan.Graphics.Shaders;
using SettlersOfCatan.Logic;

namespace SettlersOfCatan.Graphics;

public class StreetRenderer : IRenderer
{
    private Matrix4 transformField;
    public Matrix4 Transform { get => transformField; set => transformField = value; }

    public Street StreetObject { get; set; }


    private static Mesh RoadMesh { get; set; }

    private static float HalfStreetWidth => 1 / 2.4f;
    private static float HalfStreetHeight => (1f / 6f) / 2.4f;

    public static void Init()
    {
        Vertex[] streetVertices = new Vertex[4];
        streetVertices[0] = new()
        {
            Position = new(-HalfStreetWidth, -HalfStreetHeight)
        };
        streetVertices[1] = new()
        {
            Position = new(HalfStreetWidth, HalfStreetHeight)
        };
        streetVertices[2] = new()
        {
            Position = new(HalfStreetWidth, -HalfStreetHeight)
        };
        streetVertices[3] = new()
        {
            Position = new(-HalfStreetWidth, HalfStreetHeight)
        };

        int[] streetIndices = new int[6]
        {
                0, 1, 2,
                0, 1, 3,
        };

        RoadMesh = new(streetVertices, streetIndices);
        RoadMesh.SetAttributes<BoardShader>();
    }
    public static void Destroy()
        => RoadMesh.Dispose();

    public bool IsPointInShape(Vector2 point)
    {
        if (StreetObject is null)
            return false;

        Matrix4 funny = Transform; //the funny thing about this is that I don't know
        funny.Transpose(); //why this needs to be transposed (¯\_(ツ)_/¯)
        Vector2 center = (funny * new Vector4(0, 0, 0, 1)).Xy;
        Vector2 right = (funny * new Vector4(1, 0, 0, 1)).Xy;
        Vector2 centeredPoint = point - center;
        float scale = (right - center).Length;

        //unrotate street by rotating point
        Vector2 newPoint = (funny.ExtractRotation() * new Vector4(centeredPoint.X, centeredPoint.Y, 0, 1)).Xy;
        return Math.Abs(newPoint.X) < scale * HalfStreetWidth && Math.Abs(newPoint.Y) < scale * HalfStreetHeight;
    }

    public void Render()
    {
        if (StreetObject.Unused) 
            return;

        GL.UniformMatrix4(Shader.Board.TransformUniform, false, ref transformField);
        GL.Uniform1(Shader.Board.IsPlayerObjectUniform, 1);
        GL.Uniform4(Shader.Board.PlayerColorUnifom, StreetObject.Owner.Color);

        GL.Uniform1(Shader.Board.ZValueUnifom, -0.3f);

        if (StreetObject.IsStreet)
            RoadMesh.DrawElements();
    }
}