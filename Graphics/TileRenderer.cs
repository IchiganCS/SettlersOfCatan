﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using SettlersOfCatan.Graphics.Shaders;
using SettlersOfCatan.Logic;
using SettlersOfCatan.Misc;

namespace SettlersOfCatan.Graphics;

public class TileRenderer : IRenderer
{
    public Tile Tile { get; set; }
    public Matrix4 TransformField;
    public Matrix4 Transform { get => TransformField; set => TransformField = value; }

    private static Mesh Hexagon { get; set; }
    private static Mesh NumberPlate { get; set; }
    private static TextRenderer NumberTextureGenerator { get; set; }
    private static Texture[] NumberTextures { get; set; }
    private static Texture[] ResourceTextures { get; set; }
    private static Texture OceanTextures { get; set; }
    private static Texture DesertTextures { get; set; }
    private static Texture[] PortTextures { get; set; }


    public static void Init()
    {
        Logger.Info($"Initializing {nameof(Hexagon)} and {nameof(NumberPlate)}");
        //construct 6 outer points through mathematics
        Vector2[] positions = Enumerable.Range(0, 6)
            .Select(x => {
                float angle = x * 60.0f / 180.0f * (float)Math.PI;
                return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            }).ToArray();

        //these values are look weird: they correct rounding errors
        //take a look at lumber_tile: 1 should be at both top and bottom, left and right should be 1 too
        //but we half the values!
        //we stretch vertically by Math.Sin(60)
        //2.15 is a magic number to cut off some parts of the texture
        float textureCutOff = 2.15f;
        Vector2[] texCoords = positions.Select(pos => pos / textureCutOff * new Vector2(1f, 1f / (float)Math.Sin(60f / 180f * (float)Math.PI)) + new Vector2(0.5f)).ToArray();

        //side note: the height is 2f * (float)Math.Cos(30f)

        int[] indices =
        {
            0, 1, 2,
            0, 2, 3,
            0, 3, 5,
            3, 4, 5,
        };

        Vertex[] vertices = new Vertex[positions.Length];
        for (int i = 0; i < positions.Length; i++)
            vertices[i] = new()
            {
                Position = positions[i],
                TexCoord = texCoords[i],
            };

        Hexagon = new(vertices, indices);
        Hexagon.SetAttributes<BoardShader>();



        NumberTextureGenerator = new("Times New Roman");

        //create a "circle" with 20 corners
        int RESOLUTION = 16;
        vertices = new Vertex[RESOLUTION + 1];
        //to easily make triangles, we add a centerpoint
        //two neighboring vertices make a triangle to centerpoint
        //resulting in RESOLUTION vertices
        vertices[RESOLUTION] = new() //centerpoint
        {
            Position = new(),
            TexCoord = new(0.5f)
        };
        indices = new int[3 * RESOLUTION];
        for (int i = 0; i < RESOLUTION; i++)
        {
            float angle = i * 2 * (float)Math.PI / RESOLUTION;
            vertices[i] = new()
            {
                Position = new((float)Math.Cos(angle), (float)Math.Sin(angle))
            };
            vertices[i].TexCoord = vertices[i].Position / 2 + new Vector2(0.5f);
            vertices[i].Position /= 3.2f;//magic number to fit the layout of the hexagon
            indices[3 * i] = RESOLUTION;
            indices[3 * i + 1] = i;
            indices[3 * i + 2] = (i + 1) % RESOLUTION;
        }

        NumberPlate = new(vertices, indices);
        NumberPlate.SetAttributes<BoardShader>();

        NumberTextures = new Texture[11];
        for (int i = 0; i < NumberTextures.Length; i++)
        {
            if (i == 5)//there is no 7 number texture
                continue;
            int distFrom7 = Math.Abs(i - 5);
            NumberTextures[i] = 
                NumberTextureGenerator.MakeCenteredText((i + 2).ToString(), 
                    1.1f + distFrom7 * 0.18f, 100, 
                    (distFrom7 == 1) ? Color4.Red : Color4.Black, Color4.Beige);
        }

        ResourceTextures = Enum.GetNames<Resource>()
            .Select(x => new Texture($"{x.ToLower()}_tile.jpg")).ToArray();
        OceanTextures = new("ocean_tile.jpg");
        DesertTextures = new("desert_tile.jpg");
        PortTextures = Enum.GetNames<Resource>()
            .Select(x => new Texture($"port_{x.ToLower()}_tile.jpg"))
            .Append(new("port_general_tile.jpg")).ToArray();

    }
    public static void Destroy()
    {
        Logger.Info("Destroyed the vertex collections");
        Hexagon.Dispose();
        NumberPlate.Dispose();
        foreach (Texture number in NumberTextures)
            number.Dispose();
    }

    public bool IsPointInShape(Vector2 point)
    {
        Matrix4 funny = TransformField; //the funny thing about this is that I don't know
        funny.Transpose(); //why this needs to be transposed (¯\_(ツ)_/¯)
        Vector2 center = (funny * new Vector4(0, 0, 0, 1)).Xy;
        Vector2 right = (funny * new Vector4(1, 0, 0, 1)).Xy;
        Vector2 centeredPoint = point - center;

        float scale = (center - right).Length;
        float tileHeight = (float)Math.Sin(60f / 180f * Math.PI) * scale;
        float tileWidth = scale;
        float halfWidth = tileWidth * (float)Math.Cos(60f / 180f * Math.PI);

        //normalize the point to the upper right quadrant
        Vector2 normalizedPoint = new(Math.Abs(centeredPoint.X), Math.Abs(centeredPoint.Y));
        //make a smaller bounding box for 
        //a) better performance
        //b) gurantee that the point is inside the quadrant
        if (normalizedPoint.X > tileWidth || normalizedPoint.Y > tileHeight)
            return false;

        normalizedPoint.X -= halfWidth;
        //if this is true, the point is left of the beginning of the drop
        //with the guarantee from the bounding box check, the point is inside the hexagon
        if (normalizedPoint.X < 0)
            return true;

        //the point has to be below the dropping line of the hexagon
        //normalize the point so that the beginning of the fall is at 0, the end is at 1
        float xFrom1To0 = (halfWidth - normalizedPoint.X) / halfWidth;
        return normalizedPoint.Y < xFrom1To0 * tileHeight;
    }

    public void Render()
    {
        GL.UniformMatrix4(Shader.Board.TransformUniform, false, ref TransformField);
        GL.Uniform1(Shader.Board.TileSelectedUniform, Tile.IsSelected ? 1 : 0);
        GL.Uniform1(Shader.Board.IsPlayerObjectUniform, 0);
        GL.Uniform1(Shader.Board.ZValueUnifom, 0.5f);

        //find correct texture - this could be cached?
        if (Tile.IsDesert)
            DesertTextures.Bind();
        else if (Tile.IsOcean)
            OceanTextures.Bind();
        else if (Tile.IsPort)
            //this is inferior C# null recognition. IsPort checks for null directly
            PortTextures[(int)Tile.Port!].Bind();
        else if (Tile.IsResource)
            ResourceTextures[(int)Tile.Resource!].Bind();

        Hexagon.DrawElements();

        if (Tile.Number < 2 || Tile.Number > 12)
            return;

        GL.Uniform1(Shader.Board.ZValueUnifom, 0.3f);
        NumberTextures[Tile.Number - 2].Bind();

        NumberPlate.DrawElements();
    }

    public override string ToString()
    {
        return Tile.ToString();
    }
}