﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using SettlersOfCatan.Graphics.Shaders;
using SettlersOfCatan.Misc;
using System.Runtime.InteropServices;

namespace SettlersOfCatan.Graphics;

public sealed class Mesh : IDisposable
{
    private int VBO { get; set; }
    private int EBO { get; set; }
    private int VAO { get; set; }

    public Action DrawElements { get; private set; }

    private void MakeGLBuffers(in Vertex[] vertices, in int[] indices)
    {
        if (vertices is null)
        {
            Logger.Warn($"created invalid {nameof(Mesh)}");
            return;
        }

        VAO = GL.GenVertexArray();
        GL.BindVertexArray(VAO);

        VBO = GL.GenBuffer();
        GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
        GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * Marshal.SizeOf<Vertex>(), vertices, BufferUsageHint.StaticDraw);

        EBO = GL.GenBuffer();
        GL.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
        GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * Marshal.SizeOf<int>(), indices, BufferUsageHint.StaticDraw);


        int indicesCount = indices.Length;

        DrawElements = () =>
        {
            GL.BindVertexArray(VAO);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
            GL.DrawElements(PrimitiveType.Triangles, indicesCount, DrawElementsType.UnsignedInt, 0);
        };
    }

    public Mesh(in Vertex[] vertices, in int[] indices)
    {
        MakeGLBuffers(vertices, indices);
    }

    public static Mesh LoadFromOFF(string meshName, float scale = 1f)
    {
        Logger.Info($"loading OFF file \"{meshName}\"");
        string filename = ResourceManager.GetFileName(meshName, ResourceManager.FileType.Mesh);

        string[] lines = File.ReadAllLines(filename);
        if (!lines[0].StartsWith("OFF"))
        {
            Logger.Warn($"Loaded invalid OFF file \"{filename}\"");
        }
        int vertexCount = 0, indicesCount = 0;
        int nextLine = 0;
        for (int i = 1; i < lines.Length; i++)
        {
            string line = lines[i];
            if (line.StartsWith("#"))
                continue;
            if (string.IsNullOrEmpty(line))
                continue;

            string[] numbers = line.Split();
            vertexCount = Convert.ToInt32(numbers[0]);
            indicesCount = Convert.ToInt32(numbers[1]);
            nextLine = i + 1;
            break;
        }

        Vertex[] vertices = new Vertex[vertexCount];
        int[] indices = new int[indicesCount * 3]; //can only handle triangles
        int currentVert = 0;
        int currentIdx = 0;

        for (int i = nextLine; i < lines.Length; i++)
        {
            string line = lines[i];
            if (line.StartsWith("#"))
                continue;
            if (string.IsNullOrEmpty(line))
                continue;

            if (currentVert < vertices.Length)
            {
                string[] numbers = line.Split();
                vertices[currentVert++] = new()
                {
                    Position = new Vector2((float)Convert.ToDouble(numbers[0]), (float)Convert.ToDouble(numbers[1])) * scale
                };
            }
            else if (currentIdx < indices.Length)
            {
                string[] numbers = line.Split();
                if (Convert.ToInt32(numbers[0]) != 3)
                {
                    Logger.Warn("couldn't load OFF file - indices vertex count on a face not 3");
                    return null;
                }
                for (int j = 1; j < 4; j++)
                    indices[currentIdx++] = Convert.ToInt32(numbers[j]);
            }
            //finished
            else
                break;
        }

        return new(vertices, indices);
    }

    public void SetAttributes<T>() where T : Shader
    {
        GL.BindVertexArray(VAO);
        GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);

        if (typeof(T) == typeof(BoardShader))
        {
            GL.EnableVertexAttribArray(Shader.Board.PositionAttrib);
            GL.EnableVertexAttribArray(Shader.Board.TexCoordAttrib);

            int stride = Marshal.SizeOf<Vertex>();

            GL.VertexAttribPointer(Shader.Board.PositionAttrib, 2, VertexAttribPointerType.Float, false, stride, Marshal.OffsetOf<Vertex>(nameof(Vertex.Position)));
            GL.VertexAttribPointer(Shader.Board.TexCoordAttrib, 2, VertexAttribPointerType.Float, false, stride, Marshal.OffsetOf<Vertex>(nameof(Vertex.TexCoord)));
        }
    }

    public void Dispose()
    {
        Logger.Info($"Disposed a {nameof(Mesh)}");
        if (VBO != -1)
        {
            GL.DeleteBuffer(VBO);
            GL.DeleteBuffer(EBO);
            GL.DeleteVertexArray(VAO);
            VAO = -1;
            EBO = -1;
            VBO = -1;
        }
        else
        {
            Logger.Warn("Called dispose on a disposed object");
        }
    }
}