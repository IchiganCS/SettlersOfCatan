﻿using OpenTK.Mathematics;

namespace SettlersOfCatan.Graphics;

public struct Vertex
{
    public Vector2 Position;
    public Vector2 TexCoord;
}