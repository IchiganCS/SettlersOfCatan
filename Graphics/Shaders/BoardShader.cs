﻿using OpenTK.Graphics.OpenGL;
using SettlersOfCatan.Misc;


namespace SettlersOfCatan.Graphics.Shaders;

public sealed class BoardShader : Shader
{
    public int PositionAttrib { get; private set; } = -1;
    public int TexCoordAttrib { get; private set; } = -1;
    public int TransformUniform { get; private set; } = -1;
    public int AspectScaleUniform { get; private set; } = -1;
    public int TileSelectedUniform { get; private set; } = -1;
    public int IsPlayerObjectUniform { get; private set; } = -1;
    public int PlayerColorUnifom { get; private set; } = -1;
    /// <summary>
    /// keep in mind: -1 is front, 1 is back
    /// </summary>
    public int ZValueUnifom { get; private set; } = -1;

    public BoardShader() :
        base(
            File.ReadAllText(ResourceManager.GetFileName("CommonShader.vert", ResourceManager.FileType.Shader)),
            File.ReadAllText(ResourceManager.GetFileName("BoardShader.frag", ResourceManager.FileType.Shader)))
    {

        Logger.Info("Making tile shader");

        PositionAttrib = GL.GetAttribLocation(ProgramID, "posAttr");
        TexCoordAttrib = GL.GetAttribLocation(ProgramID, "texCoordAttr");

        //set uniform locations
        TransformUniform = GL.GetUniformLocation(ProgramID, "transform");
        AspectScaleUniform = GL.GetUniformLocation(ProgramID, "aspectScale");
        TileSelectedUniform = GL.GetUniformLocation(ProgramID, "tileSelected");

        IsPlayerObjectUniform = GL.GetUniformLocation(ProgramID, "isPlayerObject");
        PlayerColorUnifom = GL.GetUniformLocation(ProgramID, "playerColor");
        ZValueUnifom = GL.GetUniformLocation(ProgramID, "zValue");
    }
}
