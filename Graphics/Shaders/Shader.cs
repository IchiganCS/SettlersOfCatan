﻿using OpenTK.Graphics.OpenGL;
using SettlersOfCatan.Misc;

namespace SettlersOfCatan.Graphics.Shaders;
public abstract class Shader : IDisposable
{
    protected int ProgramID { get; init; }


    private static Shader _active = null;
    public static BoardShader Board { get; set; } = new();
    public static Shader Active
    {
        get => _active;
        set
        {
            if (_active?.ProgramID == value.ProgramID)
                return;

            _active = value;
            GL.UseProgram(_active.ProgramID);
        }
    }

    public Shader(string vertexSource, string fragmentSource)
    {
        Logger.Info("Calling abstract shader ctor");

        ProgramID = GL.CreateProgram();

        int vs = GL.CreateShader(ShaderType.VertexShader);
        GL.ShaderSource(vs, vertexSource);
        GL.CompileShader(vs);
        Logger.ReadOpenGLShaderError(vs, ShaderType.VertexShader);
        GL.AttachShader(ProgramID, vs);

        int fs = GL.CreateShader(ShaderType.FragmentShader);
        GL.ShaderSource(fs, fragmentSource);
        GL.CompileShader(fs);
        Logger.ReadOpenGLShaderError(fs, ShaderType.FragmentShader);
        GL.AttachShader(ProgramID, fs);
        
        GL.LinkProgram(ProgramID);

        GL.DetachShader(ProgramID, vs);
        GL.DetachShader(ProgramID, fs);
        GL.DeleteShader(vs);
        GL.DeleteShader(fs);
    }

    public void Dispose()
    {
        Logger.Info("Dispose shader object");
        GL.DeleteProgram(ProgramID);
        GC.SuppressFinalize(this);
    }
}