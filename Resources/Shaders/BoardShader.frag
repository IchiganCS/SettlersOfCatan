﻿#version 330 core

in vec2 posPass;
in vec2 texPass;

out vec4 fragColor;

uniform bool tileSelected;
uniform bool isPlayerObject;
uniform vec4 playerColor;
uniform sampler2D texture0;

void main()
{
    if (isPlayerObject)
        fragColor = playerColor;
    else {
        fragColor = texture(texture0, texPass);
        if (tileSelected)
            fragColor += vec4(0.2, 0.3, 0.2, 0.0) * length(posPass); //this color seems surprisingly good
    }
}