﻿#version 330 core

in vec2 posAttr;
in vec2 texCoordAttr;

uniform mat4 transform;
uniform mat4 aspectScale;

uniform float zValue;
        
out vec2 posPass;
out vec2 texPass;
        

void main()
{
    gl_Position = aspectScale * transform * vec4(posAttr, zValue, 1.0);
    texPass = texCoordAttr;
    posPass = posAttr;
}