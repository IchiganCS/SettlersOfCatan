﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;
using SettlersOfCatan.Graphics;
using SettlersOfCatan.Graphics.Shaders;
using SettlersOfCatan.Logic;
using SettlersOfCatan.Misc;
using SixLabors.ImageSharp.PixelFormats;
using System.Runtime.InteropServices;
using SharpImage = SixLabors.ImageSharp.Image;
using SharpRgbaImage = SixLabors.ImageSharp.Image<SixLabors.ImageSharp.PixelFormats.Rgba32>;
using TKImage = OpenTK.Windowing.Common.Input.Image;

namespace SettlersOfCatan;

public class Window : GameWindow
{
    public Window() :
        base(
        new GameWindowSettings()
        {
            RenderFrequency = 60,
            UpdateFrequency = 60,
        },
        new NativeWindowSettings()
        {
            Title = "Settlers of Catan",
            Size = new(1280, 960),
        })
    {
        Logger.Info("Window was constructed");
        Logger.EnableOpenGLDebugOutput();


    }

    public static void Main(string[] _)
    {
        ResourceManager.Init("Resources/");

        using Window win = new();

        SharpRgbaImage icon = SharpImage.Load<Rgba32>(ResourceManager.GetFileName("window_icon.jpg", ResourceManager.FileType.Image));
        if (icon.TryGetSinglePixelSpan(out Span<Rgba32> span))
            win.Icon = new(new TKImage(icon.Width, icon.Height, MemoryMarshal.AsBytes(span).ToArray()));
        else
            Logger.Warn("couldn't load window icon");
        icon.Dispose();

        win.Run();
    }

    private BoardGraph BoardGraph { get; set; }
    private Texture TestText { get; set; }

    protected override void OnLoad()
    {
        Logger.Info($"Called {nameof(OnLoad)}");

        TextRenderer.Init();

        TileRenderer.Init();
        JunctionRenderer.Init();
        StreetRenderer.Init();

        TestText = new TextRenderer("Times New Roman").MakeCenteredText("This is a test string!", 1f, 300, Color4.Black, Color4.Transparent);

        BoardGraph = BoardGraph.GenerateDefault();
        BoardGraph.Offset = new(-1f, -0.9f);
        BoardGraph.Size = new(2f, 1.8f);
        BoardGraph.Recalculate();

        GL.ClearColor(Color4.LightBlue);
        GL.Enable(EnableCap.DepthTest);
        GL.Enable(EnableCap.Blend);
        GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
        base.OnLoad();
    }
    protected override void OnUnload()
    {
        Logger.Info("Unload resources");
        TileRenderer.Destroy();
        Shader.Board.Dispose();
        JunctionRenderer.Destroy();
        StreetRenderer.Destroy();

        base.OnUnload();
    }

    protected override void OnUpdateFrame(FrameEventArgs args)
    {
        if (IsMouseButtonReleased(MouseButton.Button1))
        {
            MousePosition.Deconstruct(out float x, out float y);
            Matrix4 scaler = Matrix4.CreateOrthographic(Size.Y, Size.Y, 0.1f, 1.1f);
            Vector2 point = (scaler * new Vector4(x - Size.X / 2, Size.Y / 2 - y, 0.6f, 1f)).Xy;

            List<object> objects = BoardGraph.GetSelectedElements(point);
            if (!objects.Any())
                return;
            object obj = objects.Find(x => x is Junction);
            obj ??= objects.Find(x => x is Street);
            obj ??= objects.Find(x => x is Tile);
            {
                if (obj is Tile tile)
                    tile.IsSelected = !tile.IsSelected;
                else if (obj is Junction junc)
                {
                    junc.Owner = new Player() { Color = Color4.Blue };
                    junc.IsHouse = !junc.IsHouse;
                }
                else if (obj is Street street)
                {
                    street.Owner = new Player() { Color = Color4.Blue };
                    street.IsStreet = !street.IsStreet;
                }
            }
        }

        if (KeyboardState.IsKeyPressed(Keys.Space))
        {
            BoardGraph = BoardGraph.GenerateDefault();
            BoardGraph.Offset = new(-1f, -0.9f);
            BoardGraph.Size = new(2f, 1.8f);
            BoardGraph.Recalculate();
        }

        base.OnUpdateFrame(args);
    }

    protected override void OnRenderFrame(FrameEventArgs args)
    {
        GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

        Shader.Active = Shader.Board;
        //aspectScale stretches every screen space coordinate to a 4:3 aspect ratio
        //we can set any coordinate from x:(-4/3,4/3) and y:(-1,1) and it will be displayed correctly
        float correctWidth = Size.Y / 3.0f * 4.0f;
        float tooMuch = (Size.X - correctWidth) / correctWidth;
        Matrix4 aspectScale = Matrix4.CreateScale(1.0f / (tooMuch + 1) * 3f / 4, 1, 1);
        GL.UniformMatrix4(Shader.Board.AspectScaleUniform, false, ref aspectScale);


        BoardGraph.Render();
        TextRenderer.Render(TestText, Matrix4.CreateTranslation(0f, 0f, 0f));

        Context.SwapBuffers();
        base.OnRenderFrame(args);
    }

    protected override void OnResize(ResizeEventArgs e)
    {
        base.OnResize(e);
        GL.Viewport(0, 0, e.Width, e.Height);
    }
}