﻿using OpenTK.Graphics.OpenGL;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SettlersOfCatan.Misc;

public class Logger
{
    public static void EnableOpenGLDebugOutput()
    {
        Info("Enabled OpenGL debug output");
        GL.Enable(EnableCap.DebugOutput);
        GL.DebugMessageCallback((source, type, id, severity, length, message, userParam) =>
        {
            Action<string> action = severity switch
            {
                DebugSeverity.DebugSeverityHigh => Error,
                DebugSeverity.DebugSeverityMedium => Warn,
                DebugSeverity.DebugSeverityNotification => x => { }, //these really are no important things at all
                _ => Info
            };

            action($"OpenGL (source {source}): {Marshal.PtrToStringUTF8(message, length)}");
        }, (IntPtr)0);
    }

    [Obsolete($"Enable general debugging with {nameof(EnableOpenGLDebugOutput)}")]
    public static void ReadOpenGLError([CallerMemberName] string method = null, [CallerLineNumber] int line = -1)
    {
        ErrorCode error = GL.GetError();
        if (error == ErrorCode.NoError)
        {
            Info("no error found");
            return;
        }

        Warn($"Found GL error {error} in method {method}, line {line}");
    }
    public static void ReadOpenGLShaderError(int shader, ShaderType shaderType)
    {
        GL.GetShaderInfoLog(shader, out string message);
        if (string.IsNullOrEmpty(message))
            Info($"no error found in shader {shader}, type {shaderType}");
        else
            Error(message);
    }


    public static void Info(string message)
    {
#if DEBUG
        Console.WriteLine("info: " + message);
#endif
    }
    public static void InfoIf(bool condition, string message)
    {
#if DEBUG
        if (condition)
            Logger.Info(message);
#endif
    }


    public static void Warn(string message)
    {
#if DEBUG
        Console.WriteLine("Warn: " + message);
#endif
    }
    public static void WarnIf(bool condition, string message)
    {
#if DEBUG
        if (condition)
            Logger.Warn(message);
#endif
    }


    public static void Error(string message)
    {
#if DEBUG
        Console.WriteLine("ERROR: " + message);
#endif
    }
    public static void ErrorIf(bool condition, string message)
    {
#if DEBUG
        if (condition)
            Logger.Error(message);
#endif
    }
}