﻿namespace SettlersOfCatan.Misc;

public static class ResourceManager
{
    public enum FileType
    {
        Texture, Shader, Image, Mesh, Font
    }

    private static Dictionary<FileType, string> Folders { get; } = new();

    public static void Init(string root)
    {
        Logger.Info("Initalize ResourceManager");

        Folders.Add(FileType.Shader, root + "Shaders/");
        Folders.Add(FileType.Texture, root + "Textures/");
        Folders.Add(FileType.Image, root + "Images/");
        Folders.Add(FileType.Mesh, root + "Meshes/");
        Folders.Add(FileType.Font, root + "Fonts/");
    }

    public static string GetFileName(string filename, FileType folder)
    {
        return Folders[folder] + filename;
    }
}